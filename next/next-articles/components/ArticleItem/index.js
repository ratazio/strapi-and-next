import * as S from "./style";


export default function ArticleItem({ article }) {
  const { Title, Content, main_image } = article;
  return (
    <S.ArticleContainer padding={10}>
      <S.ArticleTitle>{Title}</S.ArticleTitle>
      <S.ArticleContent>{Content}</S.ArticleContent>
    </S.ArticleContainer>
  );
}
