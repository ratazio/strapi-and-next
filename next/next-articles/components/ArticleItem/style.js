import styled from "styled-components";
import ReactMarkdown from "react-markdown";

export const ArticleContainer = styled.div`
  background-color: grey;
  width: 300px;
  height: 400px;
  border-radius: 30px;
  padding: ${(props) => (props.padding ? props.padding : 10)}px;
  text-align: center;
`;

export const ArticleTitle = styled.h2`
  font-size: 1.5rem;
  text-align: center;
`;

export const ArticleContent = styled(ReactMarkdown)`
  font-size: 0.9rem;
  text-align: center;
`;
