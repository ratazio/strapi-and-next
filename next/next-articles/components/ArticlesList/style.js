import styled from "styled-components";

export const ArticlesContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-evenly;
`;
