import * as S from "./style";

import ArticleItem from "../ArticleItem";

export default function ArticleList({ articles }) {
  return (
    <S.ArticlesContainer>
      {articles.map((article, index) => (
        <ArticleItem key={index} article={article}/>
      ))}
    </S.ArticlesContainer>
  );
}
